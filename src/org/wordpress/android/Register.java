package org.wordpress.android;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.conn.HttpHostConnectException;
import org.wordpress.android.util.UserEmailFetcher;
import org.xmlrpc.android.XMLRPCClient;
import org.xmlrpc.android.XMLRPCException;
import org.xmlrpc.android.XMLRPCFault;

public class Register extends Activity implements AppSettingsInterface {
	public ProgressDialog pd;
	private XMLRPCClient client;

	// private UserEmailFetcher fetcher;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);

		String email = UserEmailFetcher.getEmail(this);

		final EditText emailField = (EditText) findViewById(R.id.email);
		emailField.setText(email);

		final Button registerButton = (Button) findViewById(R.id.register);

		registerButton.setOnClickListener(new Button.OnClickListener() {

			public void onClick(View v) {
				pd = ProgressDialog.show(Register.this,
						getResources().getText(R.string.account_setup),
						getResources().getText(R.string.attempting_configure),
						true, false);

				Thread action = new Thread() {
					public void run() {
						Looper.prepare();
						registerUser();
						Looper.loop();
					}

				};
				action.start();
			}
		});
	}

	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	protected void registerUser() {
		// TODO Auto-generated method stub
		final EditText usernameET = (EditText) findViewById(R.id.username);
		final String username = usernameET.getText().toString().trim();
		final EditText emailET = (EditText) findViewById(R.id.email);
		final String email = emailET.getText().toString().trim();

		final boolean validEmail = isEmailValid(email);
		
		if ("".equals(email) || "".equals(username)) {
			pd.dismiss();

			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
					Register.this);
			dialogBuilder.setTitle(Register.this.getResources().getText(
					R.string.required_fields));

			dialogBuilder.setMessage(getResources().getText(
					R.string.url_username_password_required));

			dialogBuilder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});

			dialogBuilder.setCancelable(true);
			dialogBuilder.create().show();
		} else if (!validEmail) {
			pd.dismiss();

			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
					Register.this);
			dialogBuilder.setTitle(Register.this.getResources().getText(
					R.string.invalid_email));

			dialogBuilder.setMessage(getResources().getText(
					R.string.invalid_email_message));

			dialogBuilder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});

			dialogBuilder.setCancelable(true);
			dialogBuilder.create().show();			
		} else {
			final String fBlogURL = BLOG_URL + "/xmlrpc.php";
			final String password = "123456";
			client = new XMLRPCClient(fBlogURL, username, password);
			
			XMLRPCMethod method = new XMLRPCMethod("wp.getUsersBlogs",
					new XMLRPCMethodCallback() {

						@Override
						public void callFinished(Object[] result) {
							final String[] blogNames = new String[result.length];
							final String[] urls = new String[result.length];
							final int[] blogIds = new int[result.length];
							final boolean[] wpcoms = new boolean[result.length];
							@SuppressWarnings("unused")
							final String[] wpVersions = new String[result.length];

						}
			
			});
			
		}
	}
	interface XMLRPCMethodCallback {
		void callFinished(Object[] result);
	}

	class XMLRPCMethod extends Thread {
		private String method;
		private Object[] params;
		private Handler handler;
		private XMLRPCMethodCallback callBack;

		public XMLRPCMethod(String method, XMLRPCMethodCallback callBack) {
			this.method = method;
			this.callBack = callBack;

			handler = new Handler();

		}

		public void call() {
			call(null);
		}

		public void call(Object[] params) {
			this.params = params;
			start();
		}

		@Override
		public void run() {
			try {
				final Object[] result;
				result = (Object[]) client.call(method, params);
				handler.post(new Runnable() {
					public void run() {
						callBack.callFinished(result);
					}
				});
			} catch (final XMLRPCFault e) {
				handler.post(new Runnable() {
					public void run() {
						// e.printStackTrace();
						pd.dismiss();
						String message = e.getMessage();
						if (message.contains("code 403")) {
							// invalid login
							Thread shake = new Thread() {
								public void run() {
									Animation shake = AnimationUtils
											.loadAnimation(Register.this,
													R.anim.shake);
									findViewById(R.id.section1).startAnimation(
											shake);
									Toast.makeText(
											Register.this,
											getResources().getString(
													R.string.invalid_login),
											Toast.LENGTH_SHORT).show();
								}
							};
							runOnUiThread(shake);
						} else {
							AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
									Register.this);
							dialogBuilder.setTitle(getResources().getText(
									R.string.connection_error));
							if (message.equals("HTTP status code: 404 != 200")) {
								message = "xmlrpc.php not found, please check your path";
							}
							dialogBuilder.setMessage(message);
							dialogBuilder.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
											// Just close the window.

										}
									});
							dialogBuilder.setCancelable(true);
							dialogBuilder.create().show();
						}
					}
				});

			} catch (final XMLRPCException e) {

				handler.post(new Runnable() {
					public void run() {
						Throwable couse = e.getCause();
						e.printStackTrace();
						pd.dismiss();
						if (couse instanceof HttpHostConnectException) {

						} else {
							AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
									Register.this);
							dialogBuilder.setTitle(getResources().getText(
									R.string.connection_error));
							String message = e.getMessage();
							if (message.equals("HTTP status code: 404 != 200")) {
								message = "xmlrpc.php not found, please check your path";
							}
							dialogBuilder.setMessage(message);
							dialogBuilder.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
											// Just close the window.

										}
									});
							dialogBuilder.setCancelable(true);
							dialogBuilder.create().show();
						}
						e.printStackTrace();

					}
				});
			}
		}

	}
}
