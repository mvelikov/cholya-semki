package com.choplyasemki.android;

public interface AppSettingsInterface {
	public static final String BLOG_URL = "http://choplyasemki.com/";
	public static final String REGISTER_URL = BLOG_URL + "wp-login.php?action=register";
}
