package com.choplyasemki.android;

public interface ServiceUpdateUIListener {

	public void updateUI(String accountID, String accountName);

	}//end interface ServiceUpdateUIListener
